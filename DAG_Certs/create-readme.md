generate keystore
---
keytool -genkey -alias daimler.com -keyalg RSA -keystore DAGKeyStore.jks -keysize 2048

add keys
---
keytool -import -trustcacerts -alias emea -file CorpDir.net-EMEA-Issuing-CA01_2018.crt -keystore KeyStore.jks
keytool -import -trustcacerts -alias intermediate -file CorpDir.net-Intermediate-CA01_2018.crt -keystore KeyStore.jks
keytool -import -trustcacerts -alias root -file Corp-Root-CA.crt -keystore DAGKeyStore.jks


