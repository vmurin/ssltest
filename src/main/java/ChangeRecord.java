

import java.util.List;
import java.util.Map;

/**
 * 
 * @author Gregor Zurowski
 *
 */
public class ChangeRecord {

	private Long changeId;

	private Map<String, List<String>> oldAttrs;

	private Map<String, List<String>> newAttrs;

	private String timestamp;

	private String mod;

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public Map<String, List<String>> getOldAttrs() {
		return oldAttrs;
	}

	public void setOldAttrs(Map<String, List<String>> oldAttrs) {
		this.oldAttrs = oldAttrs;
	}

	public Map<String, List<String>> getNewAttrs() {
		return newAttrs;
	}

	public void setNewAttrs(Map<String, List<String>> newAttrs) {
		this.newAttrs = newAttrs;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getMod() {
		return mod;
	}

	public void setMod(String mod) {
		this.mod = mod;
	}

}
