

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by adry on 26/4/16.
 * This class helps with some JSON Managing action/methods.
 */
public final class JSONHelper {

    /**
     * Defining a private constructor prevents the default parameter-less constructor from being used elsewhere in the code. Additionally,
     * we define the class as final so that it can not be extended by other classes, otherwise Sonar will still complain.
     */
    private JSONHelper() {
        // Throw an exception if this is ever called internally.
        throw new AssertionError("Instantiating helper class.");
    }


    /**
     * This method parses a JSON object into a POJO.
     *
     * @param jsonObject The @{JSONObject}.
     * @param clazz A class into which instance the passed JSON Object will be deserialized.
     * @return An object of the passed class.
     */
    public static <T> T buildResponse(final JsonObject jsonObject, final Class<T> clazz) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonObject.toString(), clazz);
    }


    public static JsonObject buildJsonFromArrayResponse(HttpResponse httpResponse) throws JsonIOException, IOException {
        try (InputStream inputStream = httpResponse.getEntity().getContent();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

            String line = "";
            StringBuilder responseStrBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                responseStrBuilder.append(line);
            }
            String newJson = " { \"results\" : " + responseStrBuilder.toString() + " } ";
            return new JsonParser().parse(newJson).getAsJsonObject();

        }
    }
    
    public static String buildStringResponse(HttpResponse httpResponse) throws  IOException {
        try (InputStream inputStream = httpResponse.getEntity().getContent();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

            String line = "";
            StringBuilder responseStrBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                responseStrBuilder.append(line);
            }
            return responseStrBuilder.toString();

        }
    }

    /**
     * Replaces ' with &#39
     *
     * @param string
     * @return
     */
    public static String escapeApostrophe(String string) {
        if (string == null) {
            return null;
        }
        return string.replaceAll("'", "&#39;");
    }
}
