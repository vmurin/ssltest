

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * 
 * @author Gregor Zurowski
 *
 */
public class ChangeRecordResponse {

	private List<ChangeRecord> changeRecord;

	private Boolean listComplete;

	private Long lastProcessedChangeId;

	private Long numberOfChangeRecords;

	public List<ChangeRecord> getChangeRecord() {
		return changeRecord;
	}

	public void setChangeRecord(List<ChangeRecord> changeRecord) {
		this.changeRecord = changeRecord;
	}

	public Boolean getListComplete() {
		return listComplete;
	}

	public void setListComplete(Boolean listComplete) {
		this.listComplete = listComplete;
	}

	public Long getLastProcessedChangeId() {
		return lastProcessedChangeId;
	}

	public void setLastProcessedChangeId(Long lastProcessedChangeId) {
		this.lastProcessedChangeId = lastProcessedChangeId;
	}

	public Long getNumberOfChangeRecords() {
		return numberOfChangeRecords;
	}

	public void setNumberOfChangeRecords(Long numberOfChangeRecords) {
		this.numberOfChangeRecords = numberOfChangeRecords;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
