import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.PrivateKeyDetails;
import org.apache.http.conn.ssl.PrivateKeyStrategy;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class SSLTest
{
  public static void main(String[] paramArrayOfString)  {

      final String host = "wiw-services-test.i.daimler.com";
      final String appId = "OWGASE.INT";
      String url = String.format("https://%s/drd-provisioning-service/apps/%s/records", host, appId);
      System.out.println("Connecting: " + url);
      //url = "https://pcwebshop.co.uk/";
      CloseableHttpResponse httpResponse = null;
      SSLContext sslContext = null;
      SSLConnectionSocketFactory sslSocketFactory = null;
      
   // keyStoreType is either "JKS" or "PKCS12"
      
    KeyStore keyStore = null;
    KeyStore trustStore = null;
	try {
		keyStore = KeyStore.getInstance("PKCS12");
		trustStore = KeyStore.getInstance("JKS");
	} catch (KeyStoreException e1) {
		e1.printStackTrace();
	}
      try {
		keyStore.load(new FileInputStream(new File("c:/CurrentWork/CINTEO/POOL-IDONEWEBINT.pfx")), "f8CFe62Ixl3B4Ppn1T0E".toCharArray());
		trustStore.load(new FileInputStream(new File("c:/CurrentWork/CINTEO/DAG_Certs/DAGKeyStore.jks")), "benzbenz".toCharArray());
	} catch (NoSuchAlgorithmException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (CertificateException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      System.out.println("HERE!");
      try {
          sslContext = SSLContexts.custom()
                  //.useProtocol("TLSv1.2")
                  .loadTrustMaterial(trustStore) //, new TrustSelfSignedStrategy()) 
                  .loadKeyMaterial(keyStore, "f8CFe62Ixl3B4Ppn1T0E".toCharArray())
                  .build();
          sslSocketFactory = new SSLConnectionSocketFactory(sslContext); 
      } catch (GeneralSecurityException e) {
          System.out.println("Error with keys: " );
          e.printStackTrace();
      } finally {
      }

      try (CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslSocketFactory).build()) {
          httpResponse = httpClient.execute(new HttpGet(url));
          if (HttpStatus.SC_OK == httpResponse.getStatusLine().getStatusCode()) {
        	  System.out.println("OUT: " + JSONHelper.buildStringResponse(httpResponse));
          } else {
        	  System.out.println("DRD service responded with error: " + httpResponse.getStatusLine().getStatusCode());
        	  System.out.println("OUT: " + JSONHelper.buildStringResponse(httpResponse));
          }
      } catch (IOException e) {
    	  System.out.println("Error contacting DRD service" + e);
    	  e.printStackTrace();
      }


      System.out.println("Successfully connected: ");
      System.exit(0);  	  
    
  }
}
